package com.apps.toolapps

import android.util.Log
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.libraries.Session
import com.apps.toolapps.modules.login.view.Login
import com.apps.toolapps.modules.main_app.view.MainAppAct
import org.jetbrains.anko.startActivity

class MainPresenter(val main: MainActivity) {
    init {
        checkingNotification()
    }

    private fun checkingNotification() {
        if (!Session(main).userId.equals("missing")) {
            if (main.intent.extras != null) {
                Log.e("InitHal", "Notif")
                val id_destination = main.intent.getStringExtra("id_destination")
                val document = main.intent.getStringExtra("document")
                if (id_destination != null) {
                    when (document) {
                        "pinjaman" -> {
                            val status = main.intent.getStringExtra("status")
                            val jenis = main.intent.getStringExtra("jenis")
                            main.startActivity<MainAppAct>(
                                "id_destination" to id_destination,
                                "document" to document,
                                "status" to status,
                                "jenis" to jenis)
                        }
                    }
                } else {
                    main.startActivity<MainAppAct>()
                }
            } else {
                Log.e("Notif", "Tidak ADa")
                main.startActivity<MainAppAct>()
            }
        } else {
            Log.e("InitHal", "No Notif")
            initContent()
        }
    }

    fun initContent() {
        Application().moveFragment(main, R.id.frameContent, Login())
    }
}