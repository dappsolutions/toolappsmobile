package com.apps.toolapps.firebase

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.apps.toolapps.MainActivity
import com.apps.toolapps.R
import com.apps.toolapps.modules.wizard.view.WizardActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService(){
    private val TAG = "FirebaseNotifService"

    internal lateinit var context: Context
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        context = this
        // TODO(developer): Handle FCM messages here.
        Log.e(TAG, "From: " + remoteMessage?.getFrom()!!)
        Log.e(TAG, "Data: " + remoteMessage?.getData())
        Log.e(TAG, "Notification: " + remoteMessage?.getNotification()!!)

        // Check if message contains a data payload.
        if (remoteMessage.getData().size > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData())
            receiveNotif(
                remoteMessage.getData(),
                remoteMessage.getNotification()!!.title,
                remoteMessage.getNotification()!!.body
            )
        }

        // Check if message contains a notification payload.
        Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification()!!.body!!)
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification()!!.body!!)
            receiveNotif(
                remoteMessage.getData(),
                remoteMessage.getNotification()!!.title,
                remoteMessage.getNotification()!!.body
            )
        }
    }

    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: $token")
        sendRegistrationToServer(token)
    }

    private fun sendRegistrationToServer(token: String?) {

    }

    private fun receiveNotif(data: Map<String, String>, title: String?, message: String?) {
        val intent = Intent(this, WizardActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

        val id_destination = data["id"]
        val document= data["document"]
        val status= data["status"]
        val jenis= data["jenis"]
        intent.putExtra("id_destination", id_destination)
        intent.putExtra("document", document)
        intent.putExtra("status", status)
        intent.putExtra("jenis", jenis)
        val pendingIntent = PendingIntent.getActivity(
            context, 0, intent,
            PendingIntent.FLAG_ONE_SHOT
        )
        buildNotif(pendingIntent, title!!, message!!)
    }

    private fun buildNotif(pendingIntent: PendingIntent, title: String, message: String) {
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(context, channelId)
            .setSmallIcon(R.drawable.logo_pln)
            .setContentTitle(title)
            .setContentText(message)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0, notificationBuilder.build())
    }
}