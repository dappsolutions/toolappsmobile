package com.apps.toolapps.libraries

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore

class FilePath{
    fun getPath(ctx:Context, uri:Uri): String? {
        val isKitkat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
        if(isKitkat && DocumentsContract.isDocumentUri(ctx, uri)){
            if(isEksternalStorageDocument(uri)){
                var docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":")
                var type = split[0]

                if("primary".equals(type, true)){
                    return Environment.getExternalStorageDirectory().toString()+"/"+split[1]
                }
            }

            else if(isDownloadsDocument(uri)){
                var id = DocumentsContract.getDocumentId(uri)
                var contentUri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"), java.lang.Long.valueOf(id))
                return getDataColumn(ctx, contentUri, null, null)
            }

            else if(isMEdiaDocument(uri)){
                var docId = DocumentsContract.getDocumentId(uri)
                var split = docId.split(":")
                var type = split[0]

                var contentUri: Uri? = null
                if("image".equals(type)){
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                } else if("video".equals(type)){
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                } else if("audio".equals(type)){
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }


                var selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(ctx, contentUri!!, selection, selectionArgs)
            }
        }

        else if("content".equals(uri.scheme, true)){
            if(isGooglePhotosUri(uri)){
                return uri.lastPathSegment
            }

            return getDataColumn(ctx, uri, null, null)
        }

        else if("file".equals(uri.scheme, true)){
            return uri.path
        }

        return null
    }

    fun getDataColumn(ctx:Context, uri:Uri, selection: String?, selectionArgs: Array<String>?): String? {
        var cursor: Cursor? = null
        var column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = ctx.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if(cursor != null && cursor.moveToFirst()){
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            if(cursor != null){
                cursor.close()
            }
        }
        return null
    }

    fun isEksternalStorageDocument(uri:Uri): Boolean {
        return "com.android.externalstorage.documents".equals(uri.authority)
    }

    fun isDownloadsDocument(uri:Uri): Boolean {
        return "com.android.providers.downloads.documents".equals(uri.authority)
    }

    fun isMEdiaDocument(uri:Uri): Boolean {
        return "com.android.providers.media.documents".equals(uri.authority)
    }

    fun isGooglePhotosUri(uri:Uri): Boolean {
        return "com.google.android.apps.photos.content".equals(uri.authority)
    }


}