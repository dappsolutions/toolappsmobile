package com.apps.toolapps.libraries;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;

public class Session {
    FragmentActivity activity;
    SharedPreferences setSession;
    Context context;

    public Session(FragmentActivity activity) {
        this.activity = activity;
        setSession = this.activity.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public Session(Context ctx) {
        this.context = ctx;
        setSession = this.context.getSharedPreferences("session",
                Context.MODE_PRIVATE);
    }

    public void setUserId(String param) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("user_id", param);
        data_ses.apply();
    }

    public void setUsername(String param) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("username", param);
        data_ses.apply();
    }

    public void setHakAkses(String param) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("hak_akses", param);
        data_ses.apply();
    }

    public void setUptId(String param) {
        SharedPreferences.Editor data_ses = setSession.edit();
        data_ses.putString("upt_id", param);
        data_ses.apply();
    }

    public String getUserId() {
        return setSession.getString("user_id", "missing");
    }

    public String getUsername() {
        return setSession.getString("username", "missing");
    }

    public String getHakAkses() {
        return setSession.getString("hak_akses", "missing");
    }

    public String getUptId() {
        return setSession.getString("upt_id", "missing");
    }

    public void clearSession() {
        setSession.edit().remove("user_id").commit();
    }

}
