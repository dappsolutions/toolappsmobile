package com.apps.toolapps.libraries

import android.view.View.GONE
import android.view.View.VISIBLE
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar

class WebClient : WebViewClient{
    var progresBar:ProgressBar

    constructor(progres:ProgressBar){
        progresBar = progres
        progresBar.visibility = VISIBLE
    }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        view?.loadUrl(url)
        return true
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        progresBar.visibility = GONE
    }
}