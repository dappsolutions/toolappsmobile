package com.apps.toolapps.libraries

import com.android.volley.VolleyError

interface ErrorCallback{
    fun errorContaint(error: VolleyError)
}