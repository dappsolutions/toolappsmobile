package com.apps.toolapps.libraries.models_db

data class DataAlatTB(
    val id:Int,
    val alat:String,
    val kode_alat:String,
    val upt:String,
    val qty:String
){
    companion object{
        const val table_alat_upt:String = "alat_upt"
        const val id:String = "id"
        const val alat:String = "alat"
        const val kode_alat:String = "kode_alat"
        const val upt:String = "upt"
        const val qty:String = "qty"
    }
}