package com.apps.toolapps.libraries

import android.content.DialogInterface

interface DialogCallback{
    fun okAction(dialog: DialogInterface, i: Int)
}