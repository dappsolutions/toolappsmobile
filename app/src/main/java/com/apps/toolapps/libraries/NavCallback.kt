package com.apps.toolapps.libraries

import android.view.MenuItem

interface NavCallback{
    fun getItemData(item:MenuItem)
}