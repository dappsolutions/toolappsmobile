package com.apps.toolapps.libraries

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.apps.toolapps.libraries.models_db.DataAlatTB
import org.jetbrains.anko.db.*

class DbLokal(ctx:Context):ManagedSQLiteOpenHelper(ctx, "inventori.db",
        null, 1) {
    companion object {
        var instance: DbLokal? = null

        @Synchronized
        fun getInstance(ctx:Context):DbLokal{
            if (instance == null){
                instance = DbLokal(ctx.applicationContext)
            }

            return instance as DbLokal
        }
    }


    override fun onCreate(db: SQLiteDatabase?) {
        createTableSalesMan(db)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(DataAlatTB.table_alat_upt, true)
    }

    //created tb Salesman
    fun createTableSalesMan(db: SQLiteDatabase?) {
        db?.createTable(DataAlatTB.table_alat_upt, true,
                DataAlatTB.id to INTEGER+ PRIMARY_KEY+ UNIQUE,
                DataAlatTB.alat to TEXT,
                DataAlatTB.kode_alat to TEXT,
                DataAlatTB.upt to TEXT,
                DataAlatTB.qty to TEXT)
    }
}

val Context.db:DbLokal
get() = DbLokal.getInstance(applicationContext)