package com.apps.toolapps.libraries

import android.content.Context
import android.content.Intent
import android.util.Base64
import android.util.Log
import java.io.File
import java.io.FileInputStream

class FileUtils{
    lateinit var context:Context
    constructor(ctx:Context){
        context = ctx
    }

    fun getBase64File(data: Intent?):HashMap<String, String>{
        val path = data?.data
        var dataPath = FilePath().getPath(context, path!!)

        val params = HashMap<String, String>()
        params["base64"] = ""
        params["filename"] = ""

        try {
            val dataGambar = dataPath?.split("/")
            params["filename"] = dataGambar!![dataGambar.size - 1]

            val fileSaya = File(dataPath)
            val inputStream = FileInputStream(fileSaya)
            val bytes = ByteArray(fileSaya.length().toInt() - 1)
            inputStream.read(bytes)
            params["base64"] = Base64.encodeToString(bytes, Base64.DEFAULT)
        }catch (ex:Exception){
            Log.e("Error", ex.message.toString())
        }

        return params
    }
}