package com.apps.toolapps.libraries

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build

class PermissionCheck {
    var context: Context

    constructor(ctx: Context) {
        context = ctx
    }

    fun checkPermissionReadExternalStorage(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var result = context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }

        return false
    }
}