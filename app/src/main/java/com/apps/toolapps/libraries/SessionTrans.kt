package com.apps.toolapps.libraries

import android.content.Context
import android.content.SharedPreferences
import android.support.v4.app.FragmentActivity

class SessionTrans {
    lateinit var activity: FragmentActivity
    lateinit  var setSession: SharedPreferences
    lateinit  var context: Context

    var alatUpt: String?
        get() = setSession.getString("alat_upt", "missing")
        set(playerName) {
            val data_ses = setSession.edit()
            data_ses.putString("alat_upt", playerName)
            data_ses.apply()
        }

    var qrCode: String?
        get() = setSession.getString("qrcode", "missing")
        set(qrcode) {
            val data_ses = setSession.edit()
            data_ses.putString("qrcode", qrcode)
            data_ses.apply()
        }

    var alat: String?
        get() = setSession.getString("alat", "missing")
        set(keyword) {
            val data_ses = setSession.edit()
            data_ses.putString("alat", keyword)
            data_ses.apply()
        }

    var idPinjaman: String?
        get() = setSession.getString("idPinjaman", "missing")
        set(keyword) {
            val data_ses = setSession.edit()
            data_ses.putString("idPinjaman", keyword)
            data_ses.apply()
        }

    var status: String?
        get() = setSession.getString("status", "missing")
        set(keyword) {
            val data_ses = setSession.edit()
            data_ses.putString("status", keyword)
            data_ses.apply()
        }

    var qty: String?
        get() = setSession.getString("qty", "missing")
        set(keyword) {
            val data_ses = setSession.edit()
            data_ses.putString("qty", keyword)
            data_ses.apply()
        }

    constructor(activity: FragmentActivity) {
        this.activity = activity
        setSession = this.activity.getSharedPreferences(
            "session",
            Context.MODE_PRIVATE
        )
    }

    constructor(ctx: Context) {
        this.context = ctx
        setSession = this.context.getSharedPreferences(
            "session",
            Context.MODE_PRIVATE
        )
    }

    fun clearSession() {
        setSession.edit().remove("alat_upt").commit()
        setSession.edit().remove("qrcode").commit()
        setSession.edit().remove("alat").commit()
        setSession.edit().remove("qty").commit()
        setSession.edit().remove("status").commit()
        setSession.edit().remove("idPinjaman").commit()
    }
}
