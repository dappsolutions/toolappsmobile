package com.apps.toolapps.modules.surat_pinjaman.presenter

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.surat_pinjaman.model.SuratPinjamModel
import com.apps.toolapps.modules.surat_pinjaman.model.SuratPinjamModelResponse
import com.apps.toolapps.modules.surat_pinjaman.view.DocumentSuratAct
import com.apps.toolapps.modules.surat_pinjaman.view.SuratPinjaman
import com.google.gson.Gson
import kotlinx.android.synthetic.main.surat_pinjaman_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.yesButton

class SuratPinjamanPresenter(val main:SuratPinjaman){
    lateinit var adapter:RvAdapter
    var id_pinjaman:String
    var status_pinjam:String
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    init {
        id_pinjaman = main.arguments!!.getString("id_pinjaman").toString()
        status_pinjam = main.arguments!!.getString("status").toString()

        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        getDataSuratPinjaman()
    }

    fun getDataSuratPinjaman() {
        val url = ApiService.URL.baseUrl(baseUrl, "surat_pinjam", "getDataSuratPinjaman")
        val params = HashMap<String, String>()
        params["id_pinjaman"] = id_pinjaman

        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DatAAlat", result)
                    msg.hideLoading()
                   try{
                       val data = gson.fromJson(result, SuratPinjamModelResponse::class.java)
                       if (data.data.size > 0) {
                           setListData(data.data)
                       } else {
                           msg.showMessage("Data Tidak Ditemukan")
                       }
                   }catch (ex:Exception){
                       Log.e("Error", ex.message.toString())
                   }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setListData(data: List<SuratPinjamModel>) {
        try{
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtFile = view.findViewById(R.id.txtFile) as TextView
                    val txtUpt = view.findViewById(R.id.txtUpt) as TextView
                    val txtStatus = view.findViewById(R.id.txtStatus) as TextView
                    val btnHapus = view.findViewById(R.id.btnHapus) as Button
                    val btnView = view.findViewById(R.id.btnView) as Button

                    txtFile.text = data[position].file
                    txtUpt.text = data[position].nama_upt
                    txtStatus.text = data[position].status

                    if(data[position].equals("") || data[position].equals("null")){
                        btnView.visibility = GONE
                    }

                    if(!session.hakAkses.equals("REQUESTOR PEMINJAMAN ALAT")){
                        btnHapus.visibility = GONE
                    }else{
                        if(!status_pinjam.equals("DRAFT")){
                            btnHapus.visibility = GONE
                        }
                    }

                    when(data[position].status){
                        "Normal" -> {
                            txtStatus.setTextColor(main.resources.getColorStateList(R.color.btn_success))
                        }
                        "Emergency" ->{
                            txtStatus.setTextColor(main.resources.getColorStateList(R.color.btn_error))
                        }
                    }


                    btnHapus.setOnClickListener {
                        main.alert("Apa anda yakin menghapus data ini ?") {
                            yesButton {
                                execDelete(data, position)
                            }

                            noButton {
                                it.dismiss()
                            }
                        }.show()
                    }

                    if(data[position].file == null || data[position].file.equals("")){
                        btnView.visibility = GONE
                    }

                    btnView.setOnClickListener {
                        //show pdf file read
                        var file = data[position].file
//                        Log.e("DATAFILE", file)
                        main.startActivity<DocumentSuratAct>("file" to file)
                    }

                }
            }, main.activity!!, R.layout.list_surat_pinjam)
            Helper(main.activity, main.rvListSurat, adapter).setRecycleview()
        }catch (ex:Exception){
            Log.e("Error", ex.message.toString())
        }
    }

    fun execDelete(data: List<SuratPinjamModel>, position: Int) {
        val url = ApiService.URL.baseUrl(baseUrl, "surat_pinjam", "execDelete")
        val params = HashMap<String, String>()
        params["id"] = data[position].id

        msg.showMessage("Proses Hapus Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    val lisData = data as ArrayList<Any?>
                    lisData.removeAt(position)
                    adapter.notifyDataSetChanged()
                    msg.showMessage("Berhasil Dihapus")
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }
}