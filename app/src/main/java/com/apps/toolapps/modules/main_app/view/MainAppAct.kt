package com.apps.toolapps.modules.main_app.view

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity.LEFT
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.apps.toolapps.MainActivity
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.libraries.models_db.DataAlatTB
import com.apps.toolapps.modules.dashboard.view.Dashboard
import com.apps.toolapps.modules.internal.view.HistoriPengajuan
import com.apps.toolapps.modules.internal.view.Internal
import com.apps.toolapps.modules.main_app.presenter.MainAppPresenter
import com.apps.toolapps.modules.notification.view.Notifikasi
import kotlinx.android.synthetic.main.main_app_activity.*
import org.jetbrains.anko.db.delete

class MainAppAct : AppCompatActivity() {
    lateinit var presenter:MainAppPresenter
    lateinit var upt_trans:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_app_activity)
        setSupportActionBar(toolbar)

        presenter = MainAppPresenter(this)

        initMenu()

        setUserProfil()

        toolbar.setNavigationOnClickListener {
            drawer_layout.openDrawer(LEFT)
        }
    }

    fun setUserProfil() {
        val header = nav_bar.getHeaderView(0)
        val txtUsername = header.findViewById(R.id.txtUsername) as TextView
        txtUsername.text = Session(this).hakAkses
    }

    fun initMenu(){
        Helper(nav_bar).setNavigationDrawer(object : NavCallback {
            override fun getItemData(item: MenuItem) {
                drawer_layout.closeDrawers()
                when(item.itemId){
                    R.id.nav_dashboard -> {
                        val dashboard = Dashboard()
                        Application().moveFragment(this@MainAppAct, R.id.frameContent, dashboard)
                    }

                    R.id.nav_internal -> {
                        db.use {
                            delete(DataAlatTB.table_alat_upt, "${DataAlatTB.alat} != 0")
                        }
                        val formPengajuan = Internal()
                        val bundle = Bundle()
                        bundle.putString("jenis", "internal")
                        formPengajuan.arguments = bundle
                        Application().moveFragment(this@MainAppAct, R.id.frameContent, formPengajuan)
                    }
                    R.id.nav_eksternal -> {
                        db.use {
                            delete(DataAlatTB.table_alat_upt, "${DataAlatTB.alat} != 0")
                        }
                        val formPengajuan = Internal()
                        val bundle = Bundle()
                        bundle.putString("jenis", "eksternal")
                        formPengajuan.arguments = bundle
                        Application().moveFragment(this@MainAppAct, R.id.frameContent, formPengajuan)
                    }

                    R.id.nav_histori_int -> {
                        val histori = HistoriPengajuan()
                        val bundle = Bundle()
                        bundle.putString("jenis", "internal")
                        histori.arguments = bundle
                        Application().moveFragment(this@MainAppAct, R.id.frameContent, histori)
                    }

                    R.id.nav_histori_eks -> {
                        val histori = HistoriPengajuan()
                        val bundle = Bundle()
                        bundle.putString("jenis", "eksternal")
                        histori.arguments = bundle
                        Application().moveFragment(this@MainAppAct, R.id.frameContent, histori)
                    }

                    R.id.nav_sign_out ->{
                        Session(this@MainAppAct).clearSession()
                        this@MainAppAct.finish()
                        val login = Intent(this@MainAppAct, MainActivity::class.java)
                        login.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                        startActivity(login)
                    }
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.nav_menu_notif, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId

        when(id){
            R.id.ic_notification ->{
                //goto notification
                Application().moveFragment(this, R.id.frameContent, Notifikasi())
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        presenter.setCounterNotifikasi(menu)
        return super.onPrepareOptionsMenu(menu)
    }

}
