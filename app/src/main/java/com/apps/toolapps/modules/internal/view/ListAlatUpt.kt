package com.apps.toolapps.modules.internal.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.internal.presenter.ListAlatUptPresenter
import kotlinx.android.synthetic.main.alat_upt_view.*

class ListAlatUpt : Fragment(){
    lateinit var presenter:ListAlatUptPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.alat_upt_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ListAlatUptPresenter(this)

        btnAdd.setOnClickListener {
            presenter.simpan()
        }
    }
}