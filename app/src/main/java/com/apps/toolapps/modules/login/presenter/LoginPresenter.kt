package com.apps.toolapps.modules.login.presenter

import android.text.TextUtils
import android.util.Log
import com.android.volley.VolleyError
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.login.view.Login
import com.apps.toolapps.modules.main_app.view.MainAppAct
import kotlinx.android.synthetic.main.login.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.startActivity
import org.json.JSONObject

class LoginPresenter(val main: Login) {
    val baseUrl = ApiService.URL.BaseURl
    val network: Network
    val msg: Message
    val errmsg: Error
    val session: Session

    init {
        network = Network(main.context)
        msg = Message(main.context)
        errmsg = Error()
        session = Session(main.context)

    }

    fun login() {
        var username = main.edtUsername.text
        var password = main.edtPassword.text

        var is_valid = 0
        if (TextUtils.isEmpty(username)) {
            main.edtUsername.setError("Harus diisi")
            is_valid += 1
        }

        if (TextUtils.isEmpty(password)) {
            main.edtPassword.setError("Harus diisi")
            is_valid += 1
        }

        if (is_valid > 0) {
            return
        } else {
            execLogin()
        }
    }

    fun execLogin() {
        var url = ApiService.URL.baseUrl(baseUrl, "login", "sign_in")
        val params = HashMap<String, String>()
        params["username"] = main.edtUsername.text.toString()
        params["password"] = main.edtPassword.text.toString()

        msg.showLoading("Proses Login...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Login", result)
                    msg.hideLoading()

                    try{
                        val jObj = JSONObject(result)
                        if (jObj.get("is_valid").toString().equals("true")) {
                            session.userId = jObj.get("user_id").toString()
                            session.username = jObj.get("username").toString()
                            session.hakAkses = jObj.get("hak_akses").toString()
                            session.uptId = jObj.get("upt_id").toString()
                            main.startActivity<MainAppAct>()
                        } else {
                            msg.showMessage("Login Gagal")
                        }
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }
}