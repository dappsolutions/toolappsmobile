package com.apps.toolapps.modules.internal.presenter

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.libraries.models_db.DataAlatTB
import com.apps.toolapps.modules.internal.model.AlatModel
import com.apps.toolapps.modules.internal.model.UptModel
import com.apps.toolapps.modules.internal.model.UptModelResponse
import com.apps.toolapps.modules.internal.view.FormInternal
import com.apps.toolapps.modules.internal.view.ListAlatUpt
import com.google.gson.Gson
import kotlinx.android.synthetic.main.form_internal_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.select
import org.json.JSONArray
import org.json.JSONObject

class FormInternPresenter(val main: FormInternal) {
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    lateinit var adapter: RvAdapter

    var data: ArrayList<AlatModel>
    var listUpt: ArrayList<String>

    lateinit var jsonArray: JSONArray

    val msg: Message
    var status_pinjam: String =""
    var pinjaman_id: String = ""
    var jenis_pengajuan:String

    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        data = ArrayList()
        listUpt = ArrayList()

        jenis_pengajuan = main.arguments?.getString("jenis").toString()
        getListUpt()
    }

    fun getDataAlat() {
       try{
           adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
               override fun onExecuteViewHolder(view: View, position: Int) {
                   val txtKodeAlat = view.findViewById(R.id.txtKodeAlat) as TextView
                   val txtUpt = view.findViewById(R.id.txtUpt) as TextView
                   val txtJumlah = view.findViewById(R.id.txtJumlah) as TextView
                   val btnBatal = view.findViewById(R.id.btnBatal) as Button

                   txtKodeAlat.text = data[position].kode_alat
                   txtUpt.text = data[position].nama_upt
                   txtJumlah.text = "Jumlah (" + data[position].qty + ")"

                   btnBatal.setOnClickListener {

                       main.activity?.db?.use {
                           var id = data[position].id
                           delete(DataAlatTB.table_alat_upt, "${DataAlatTB.alat} = '$id'")
                       }
                       msg.showMessage(data[position].kode_alat + " Dibatalkan")

                       data.removeAt(position)
                       adapter.notifyDataSetChanged()
                   }
               }
           }, main.activity!!, R.layout.list_alat)
           Helper(main.activity, main.rvListAlat, adapter).setRecycleview()
       }catch (ex:Exception){
           Log.e("Error", ex.message.toString())
       }
    }

    fun setTanggal() {
        main.edtTanggal.setOnClickListener {
            helper.setDatePickerDialog(main.edtTanggal)
        }
    }

    fun getListUpt() {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getListUpt")
        val params = HashMap<String, String>()

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    try{
                        val data = gson.fromJson(result, UptModelResponse::class.java)
                        setListUpt(data.data)
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setListUpt(data: List<UptModel>) {
        listUpt.clear()
        listUpt.add("--Pilih Upt--")
        for(i in 0..data.size -1){
            listUpt.add(data[i].upt)
        }

        val adapterList = ArrayAdapter<String>(main.context, R.layout.list_spinner, listUpt)
        adapterList.setDropDownViewResource(R.layout.list_spinner)
        main.spUpt.adapter = adapterList

        main.spUpt.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (!main.spUpt.selectedItem.toString().equals("--Pilih Upt--")) {
                    msg.showMessage(main.spUpt.selectedItem.toString())
                    val listalat = ListAlatUpt()
                    val bundle = Bundle()
                    bundle.putString("upt", main.spUpt.selectedItem.toString())
                    listalat.arguments = bundle
                    Application().moveFragment(main.activity, R.id.frameContent, listalat)
                }
            }
        })
    }

    fun getListAlatUpt() {
        main.activity?.db?.use {
            val sql = select(DataAlatTB.table_alat_upt)
            val data_alat = sql.parseList(classParser<DataAlatTB>())
            Log.e("DAtaAlat", data_alat.toString())
            if (!data_alat.isEmpty()) {
                for (i in 0..data_alat.size - 1) {
                    data.add(
                        AlatModel(
                            data_alat[i].alat,
                            data_alat[i].kode_alat,
                            data_alat[i].upt,
                            data_alat[i].qty,
                            "",
                            ""
                        )
                    )
                    getDataAlat()
                }
            }
        }
    }

    fun simpan() {
        jsonArray = JSONArray()
        var is_valid = 0
        val edtTanggal = main.edtTanggal.text
        if (TextUtils.isEmpty(edtTanggal)) {
            main.edtTanggal.setError("Harus Diisi")
            is_valid += 1
        }

        main.activity?.db?.use {
            val sql = select(DataAlatTB.table_alat_upt)
            var dataAlatUpt = sql.parseList(classParser<DataAlatTB>())

            if (dataAlatUpt.isEmpty()) {
                msg.showMessage("Tidak Ada Alat yang dipinjam")
                is_valid += 1
            } else {
                for (i in 0..dataAlatUpt.size - 1) {
                    val jsonObject = JSONObject()
                    jsonObject.put("alat", dataAlatUpt[i].alat)
                    jsonObject.put("kode_alat", dataAlatUpt[i].kode_alat)
                    jsonObject.put("qty", dataAlatUpt[i].qty)
                    jsonObject.put("upt", dataAlatUpt[i].upt)

                    jsonArray?.put(jsonObject)
                }
            }
        }

        if (is_valid > 0) {
            return
        } else {
            execSimpan()
        }
    }

    fun execSimpan() {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "simpan")
        val params = HashMap<String, String>()
        params["user"] = session.userId
        params["tanggal"] = main.edtTanggal.text.toString()
        params["keterangan"] = main.edtKet.text.toString()
        params["status_dokumen"] = "1"

        if(jenis_pengajuan.equals("eksternal")){
            params["status_dokumen"] = "2"
        }
        params["pinjam_alat"] = jsonArray.toString()

        msg.showLoading("Proses Menyimpan Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    Log.e("dataPinjam", result)

                    try{
                        val jObj = JSONObject(result)
                        if (jObj.get("is_valid").toString().equals("true")) {
                            msg.showMessage("Pengajuan Sukses")
                            pinjaman_id = jObj.get("pinjaman").toString()
                            status_pinjam = "DRAFT"
                            setDetailForm()
                            getDataAlatAfterSimpan()
                        } else {
                            msg.showMessage("Proses Simpan Gagal")
                        }
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    @SuppressLint("RestrictedApi")
    fun setDetailForm() {
        main.edtTanggal.isEnabled = false
        main.edtKet.isEnabled = false

        main.btnSimpan.visibility = GONE
        main.btnUpload.visibility = VISIBLE
    }

    fun getDataAlatAfterSimpan() {
        try{
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtKodeAlat = view.findViewById(R.id.txtKodeAlat) as TextView
                    val txtUpt = view.findViewById(R.id.txtUpt) as TextView
                    val txtJumlah = view.findViewById(R.id.txtJumlah) as TextView
                    val btnScan = view.findViewById(R.id.btnScan) as Button

                    txtKodeAlat.text = data[position].kode_alat
                    txtUpt.text = data[position].nama_upt
                    txtJumlah.text = "Jumlah (" + data[position].qty + ")"

                    btnScan.visibility = GONE
                }
            }, main.activity!!, R.layout.list_detail_alat)
            Helper(main.activity, main.rvListAlat, adapter).setRecycleview()
        }catch (ex:Exception){
            Log.e("error", ex.message.toString())
        }
    }

}