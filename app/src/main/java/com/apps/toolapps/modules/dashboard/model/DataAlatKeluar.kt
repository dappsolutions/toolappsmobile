package com.apps.toolapps.modules.dashboard.model

import com.google.gson.annotations.SerializedName

data class DataAlatKeluar (
    @SerializedName("id")
    var id:String,
    @SerializedName("pinjaman")
    var pinjaman:String,
    @SerializedName("no_peminjaman")
    var no_peminjaman:String,
    @SerializedName("tanggal_pinjam")
    var tanggal_pinjam:String,
    @SerializedName("nama_pegawai")
    var nama_pegawai:String,
    @SerializedName("status_alat")
    var status_alat:String,
    @SerializedName("kode_alat")
    var kode_alat:String,
    @SerializedName("alat")
    var alat:String,
    @SerializedName("qty")
    var qty:String,
    @SerializedName("nama_upt")
    var nama_upt:String,
    @SerializedName("status")
    var status:String
)

data class DataAlatKeluarResponse(val data:List<DataAlatKeluar>)