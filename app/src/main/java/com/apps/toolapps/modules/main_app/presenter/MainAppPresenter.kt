package com.apps.toolapps.modules.main_app.presenter

import android.os.Bundle
import android.util.Log
import android.view.Menu
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.dashboard.view.Dashboard
import com.apps.toolapps.modules.internal.view.Detail
import com.apps.toolapps.modules.main_app.view.MainAppAct
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainAppPresenter(val main: MainAppAct) {
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message

    init {
        helper = Helper(main)
        msg = Message(main)
        session = Session(main)
        network = Network(main.applicationContext)
        errmsg = Error()
        gson = Gson()

        setTokenUser()

        checkingNotification()

    }

    private fun checkingNotification() {
        if (main.intent.extras != null) {
            Log.e("InitHal", "Notif")
            val id_destination = main.intent.getStringExtra("id_destination")
            val document = main.intent.getStringExtra("document")
            if (id_destination != null) {
                when (document) {
                    "pinjaman" -> {
                        val status = main.intent.getStringExtra("status")
                        val jenis = main.intent.getStringExtra("jenis")
                        //detail pinjaman
                        val bundle = Bundle()
                        bundle.putString("id_pengajuan", id_destination)
                        bundle.putString("status", status)
                        bundle.putString("jenis", jenis)
                        val detail = Detail()
                        detail.arguments = bundle

                        Application().moveFragment(main, R.id.frameContent, detail)
                    }
                }
            } else {
                Application().moveFragment(main, R.id.frameContent, Dashboard())
            }
        } else {
            Log.e("Notif", "Tidak ADa")
            Application().moveFragment(main, R.id.frameContent, Dashboard())
        }
    }

    fun setTokenUser() {
        Log.e("Token", FirebaseInstanceId.getInstance().getToken().toString())
        val url = ApiService.URL.baseUrl(baseUrl, "main", "setTokenUser")
        val params = HashMap<String, String>()
        params["user"] = session.userId
        params["token"] = FirebaseInstanceId.getInstance().getToken().toString()
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Token", result)
                }

                override fun onError(error: VolleyError) {
                    Log.e("Gagal Upte", error.message.toString())
                }
            })
        }
    }

    fun setCounterNotifikasi(menu: Menu?) {
        val txtCounter = menu?.findItem(R.id.ic_counter)

        val url = ApiService.URL.baseUrl(baseUrl, "notifikasi", "jumlahNotif")
        val params = HashMap<String, String>()
        params["user"] = session.userId

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    txtCounter?.title = result
                }

                override fun onError(error: VolleyError) {
                    Log.e("ErrorCounter", error.message.toString())
                }
            })
        }
    }
}