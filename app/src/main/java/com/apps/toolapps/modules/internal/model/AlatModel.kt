package com.apps.toolapps.modules.internal.model

import com.google.gson.annotations.SerializedName

data class AlatModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("kode_alat")
    var kode_alat:String,
    @SerializedName("nama_upt")
    var nama_upt:String,
    @SerializedName("qty")
    var qty:String,
    @SerializedName("alat")
    var alat:String,
    @SerializedName("status_alat")
    var status_alat:String
)

data class AlatModelResponse(val data:List<AlatModel>)