package com.apps.toolapps.modules.notification.presenter

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.notification.model.NotifModel
import com.apps.toolapps.modules.notification.model.NotifModelResponse
import com.apps.toolapps.modules.notification.view.Notifikasi
import com.google.gson.Gson
import kotlinx.android.synthetic.main.notification_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class NotifikasiPresenter (val main:Notifikasi){
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    lateinit var adapter: RvAdapter
    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        getDataNotifikasi()
    }

    fun getDataNotifikasi() {
        main.contentStatusData.visibility = GONE
        val url = ApiService.URL.baseUrl(baseUrl, "notifikasi", "getDataNotifikasi")
        val params = HashMap<String, String>()
        params["user"] = session.userId

        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Notifikasi", result)
                    msg.hideLoading()

                    try{
                        val data = gson.fromJson(result, NotifModelResponse::class.java)
                        if (data.data.size > 0) {
                            setDetailData(data.data)
                        } else {
                            main.contentStatusData.visibility = VISIBLE
//                        msg.showMessage("Data Tidak Ditemukan")
                        }
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
//                    msg.showMessage(err)
                    main.contentStatusData.visibility = VISIBLE
                    main.imgStatus.setImageResource(R.drawable.ic_page_not_found)
                    main.txtStatusData.text = err
                }
            })
        }
    }

    fun setDetailData(data: List<NotifModel>) {
        try{
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtPesan = view.findViewById(R.id.txtPesan) as TextView
                    txtPesan.text = data[position].pesan
                }
            }, main.activity!!, R.layout.list_notification)
            Helper(main.activity, main.rvListNotif, adapter).setRecycleview()
        }catch (ex:Exception){
            Log.e("error", ex.message.toString())
        }
    }
}