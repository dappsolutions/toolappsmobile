package com.apps.toolapps.modules.internal.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.libraries.Session
import com.apps.toolapps.libraries.SessionTrans
import com.apps.toolapps.modules.internal.presenter.DetailPresenter
import com.apps.toolapps.modules.surat_pinjaman.view.SuratPinjaman
import kotlinx.android.synthetic.main.detail_pengajuan_view.*

class Detail : Fragment(){
    lateinit var presenter:DetailPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.detail_pengajuan_view, container, false)
        return view
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = DetailPresenter(this)

        if(!Session(context).hakAkses.equals("REQUESTOR PEMINJAMAN ALAT")){
            if(presenter.status_pinjam.equals("DRAFT")){
                btnApprove.visibility = VISIBLE
                btnReject.visibility = VISIBLE
            }
        }

        btnApprove.setOnClickListener {
            presenter.approveAsman("APPROVE")
        }

        btnReject.setOnClickListener {
            val reject = RejectPengajuan()
            val bundle = Bundle()
            bundle.putString("id_pengajuan", presenter.id_pengajuan)
            reject.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, reject)
        }


        if(presenter.jenis_pengajuan.equals("eksternal")){
            txtTitle.text = "Detail Pengajuan Ekternal"
        }

        btnUpload.setOnClickListener {
            val suratPinjam = SuratPinjaman()
            var bundle = Bundle()
            bundle.putString("id_pinjaman", presenter.id_pengajuan)
            bundle.putString("status", presenter.status_pinjam)
            suratPinjam.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, suratPinjam)
        }
    }

    override fun onResume() {
        super.onResume()
        var qrCode = SessionTrans(context!!).qrCode
        Log.e("Resume Qrcode", qrCode)
        if(!qrCode.equals("missing")){
            presenter.scanAlat(qrCode)
        }
    }
}