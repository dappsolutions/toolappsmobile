package com.apps.toolapps.modules.internal.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Message
import com.apps.toolapps.libraries.SessionTrans
import com.google.android.gms.vision.barcode.Barcode
import info.androidhive.barcode.BarcodeReader

class ScanQrActivity : AppCompatActivity(), BarcodeReader.BarcodeReaderListener {

    lateinit var barcodeReader:BarcodeReader
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr)

        barcodeReader = supportFragmentManager.findFragmentById(R.id.barcode_scanner) as BarcodeReader

    }

    override fun onBitmapScanned(sparseArray: SparseArray<Barcode>?) {

    }

    override fun onScannedMultiple(barcodes: MutableList<Barcode>?) {

    }

    override fun onCameraPermissionDenied() {
        Message(this).showMessage("Kamera Tidak Diijinkan")
    }

    override fun onScanned(barcode: Barcode?) {
        barcodeReader.playBeep()
        Log.e("BArcode", barcode?.displayValue)
        SessionTrans(this).qrCode = barcode?.displayValue
        SessionTrans(this).alat = intent.getStringExtra("alat")
        SessionTrans(this).qty = intent.getStringExtra("qty")
        SessionTrans(this).status = intent.getStringExtra("status")
        SessionTrans(this).idPinjaman = intent.getStringExtra("idPinjaman")
        this.finish()
    }

    override fun onScanError(errorMessage: String?) {
        Message(this).showMessage("Gagal Scan")
    }
}
