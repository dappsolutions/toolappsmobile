package com.apps.toolapps.modules.surat_pinjaman.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.libraries.Session
import com.apps.toolapps.modules.surat_pinjaman.presenter.SuratPinjamanPresenter
import kotlinx.android.synthetic.main.surat_pinjaman_view.*

class SuratPinjaman : Fragment(){
    lateinit var presenter:SuratPinjamanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.surat_pinjaman_view, container, false)
        return view
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = SuratPinjamanPresenter(this)

        if(!Session(context).hakAkses.equals("REQUESTOR PEMINJAMAN ALAT")){
            btnAdd.visibility = GONE
        }else{
            if(!presenter.status_pinjam.equals("DRAFT")){
                btnAdd.visibility = GONE
            }
        }

        btnAdd.setOnClickListener {
            val formSurat = FormSuratPinjam()
            var bundle = Bundle()
            bundle.putString("id_pinjaman", presenter.id_pinjaman)
            formSurat.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, formSurat)
        }
    }
}