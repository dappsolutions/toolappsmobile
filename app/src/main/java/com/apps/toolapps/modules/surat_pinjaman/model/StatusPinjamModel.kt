package com.apps.toolapps.modules.surat_pinjaman.model

import com.google.gson.annotations.SerializedName

data class StatusPinjamModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("status")
    var status:String
)

data class StatusPinjamModelResponse(val data:List<StatusPinjamModel>)