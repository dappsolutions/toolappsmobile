package com.apps.toolapps.modules.login.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.login.presenter.LoginPresenter
import kotlinx.android.synthetic.main.login.*

class Login : Fragment(){
    lateinit var presenter:LoginPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.login, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = LoginPresenter(this)

        cardLogin.setOnClickListener {
            presenter.login()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        activity?.finish()
    }
}