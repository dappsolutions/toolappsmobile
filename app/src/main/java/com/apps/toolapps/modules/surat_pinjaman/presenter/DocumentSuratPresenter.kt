package com.apps.toolapps.modules.surat_pinjaman.presenter

import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import com.apps.toolapps.libraries.ApiService
import com.apps.toolapps.libraries.Helper
import com.apps.toolapps.modules.surat_pinjaman.view.DocumentSuratAct
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.document_surat_view.*

class DocumentSuratPresenter(val main:DocumentSuratAct){
    var file:String
    val BaseURlBerkas = ApiService.URL.BaseURlBerkas
    init {
        file = main.intent.getStringExtra("file").toString().trim()
//        file = file.replace(" ", "_").trim()
        Log.e("File", file)

        var type_file = file.replace(".pdf", "")
        type_file = type_file.replace(".png", "")
        type_file = type_file.replace(".jpg", "")
        type_file = file.replace(type_file, "").trim()

        var file_url = ApiService.URL.baseUrl(BaseURlBerkas, "files/berkas/surat_pinjaman", file)
        if(!type_file.equals(".pdf")){
            main.documentView.visibility = GONE
            main.imgFile.visibility = VISIBLE
            readFileImg(file_url)
        }else{
            main.documentView.visibility = VISIBLE
            main.imgFile.visibility = GONE
            readDocument(file_url)
        }

    }

    fun readFileImg(file_url: String) {
        Picasso.with(main.applicationContext).load(file_url).into(main.imgFile)
    }

    fun readDocument(file_url: String) {
        Log.e("File URL", file_url)
        Helper(main.documentView).readPdf(file_url)
    }
}