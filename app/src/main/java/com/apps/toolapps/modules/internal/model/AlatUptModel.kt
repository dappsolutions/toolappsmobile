package com.apps.toolapps.modules.internal.model

import com.google.gson.annotations.SerializedName

data class AlatUptModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("kode_alat")
    var kode_alat:String,
    @SerializedName("stok")
    var stok:String
)

data class AlatUptModelResponse(val data:List<AlatUptModel>)