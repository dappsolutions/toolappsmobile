package com.apps.toolapps.modules.internal.model

import com.google.gson.annotations.SerializedName

data class UptModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("upt")
    var upt:String
)

data class UptModelResponse(val data:List<UptModel>)
