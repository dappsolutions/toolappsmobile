package com.apps.toolapps.modules.internal.model

import com.google.gson.annotations.SerializedName

data class PengajuanModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("no_peminjaman")
    var no_peminjaman:String,
    @SerializedName("tanggal_pinjam")
    var tanggal_pinjam:String,
    @SerializedName("status_dokumen")
    var status_dokumen:String,
    @SerializedName("keterangan")
    var keterangan:String,
    @SerializedName("upt")
    var upt:String,
    @SerializedName("status")
    var status:String,
    @SerializedName("nama_pegawai")
    var nama_pegawai:String
)

data class PengajuanModelResponse(val data:List<PengajuanModel>)