package com.apps.toolapps.modules.notification.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.modules.notification.presenter.NotifikasiPresenter

class Notifikasi : Fragment(){
    lateinit var presenter:NotifikasiPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.notification_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = NotifikasiPresenter(this)

        //back press
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener(object : View.OnKeyListener{
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    Application().firstActivity(context)
                    return true
                }
                return false
            }
        })
    }
}