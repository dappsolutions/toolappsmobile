package com.apps.toolapps.modules.surat_pinjaman.model

import com.google.gson.annotations.SerializedName

data class SuratPinjamModel(
    @SerializedName("id")
    var id:String,
    @SerializedName("file")
    var file:String,
    @SerializedName("status")
    var status:String,
    @SerializedName("nama_upt")
    var nama_upt:String
)

data class SuratPinjamModelResponse(val data:List<SuratPinjamModel>)