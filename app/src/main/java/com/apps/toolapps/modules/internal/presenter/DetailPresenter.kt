package com.apps.toolapps.modules.internal.presenter


import android.annotation.SuppressLint
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.internal.model.AlatModel
import com.apps.toolapps.modules.internal.model.AlatModelResponse
import com.apps.toolapps.modules.internal.model.PengajuanModelResponse
import com.apps.toolapps.modules.internal.view.Detail
import com.apps.toolapps.modules.internal.view.ScanQrActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.detail_pengajuan_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.support.v4.startActivity
import org.json.JSONObject

class DetailPresenter(val main: Detail) {
    var id_pengajuan: String
    var jenis_pengajuan:String
    var status_pinjam: String
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    val sessionTrans: SessionTrans
    lateinit var adapter: RvAdapter

    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        sessionTrans = SessionTrans(main.context!!)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

//        sessionTrans.clearSession()
        id_pengajuan = main.arguments!!.getString("id_pengajuan")
        status_pinjam = main.arguments!!.getString("status").toString()
        jenis_pengajuan = main.arguments!!.getString("jenis").toString()

        checkStatusPeminjaman(id_pengajuan)
        getFormPengajuan()
        getListAlatDipinjam()
    }

    fun checkStatusPeminjaman(id: String?) {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "checkStatusPeminjaman/$id")
        val params = HashMap<String, String>()

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                @SuppressLint("RestrictedApi")
                override fun onSuccess(result: String) {
                    Log.e("Status", result)
                    if(!result.equals("DRAFT")){
                        main.btnApprove.visibility = GONE
                        main.btnReject.visibility = GONE
                    }
                }

                override fun onError(error: VolleyError) {
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun getListAlatDipinjam() {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getListAlatDipinjam")
        val params = HashMap<String, String>()
        params["id_pengajuan"] = id_pengajuan

        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DatAAlat", result)
                    msg.hideLoading()
                    try{
                        val data = gson.fromJson(result, AlatModelResponse::class.java)
                        if (data.data.size > 0) {
                            setListAlat(data.data)
                        } else {
                            msg.showMessage("Data Tidak Ditemukan")
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setListAlat(data: List<AlatModel>) {
        try{
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtKodeAlat = view.findViewById(R.id.txtKodeAlat) as TextView
                    val txtUpt = view.findViewById(R.id.txtUpt) as TextView
                    val txtJumlah = view.findViewById(R.id.txtJumlah) as TextView
                    val txtStatusAlat = view.findViewById(R.id.txtStatusAlat) as TextView
                    val btnScan = view.findViewById(R.id.btnScan) as Button

                    txtKodeAlat.text = data[position].kode_alat
                    txtUpt.text = data[position].nama_upt
                    txtJumlah.text = "Jumlah (" + data[position].qty + ")"

                    txtStatusAlat.text = data[position].status_alat
                    when(data[position].status_alat){
                        "NOT TAKEN" ->{
                            txtStatusAlat.setTextColor(main.resources.getColorStateList(R.color.btn_warning))
                        }
                        "TAKEN" ->{
                            txtStatusAlat.setTextColor(main.resources.getColorStateList(R.color.btn_success))
                        }
                        "BACK" ->{
                            txtStatusAlat.setTextColor(main.resources.getColorStateList(R.color.btn_info))
                        }
                    }

                    if(data[position].status_alat.equals("BACK") || !session.hakAkses.equals("TOOLSMAN")){
                        btnScan.visibility = GONE
                    }

                    btnScan.setOnClickListener {
                        //scan data activity
                        sessionTrans.clearSession()
                        msg.showMessage("Scan")
                        main.startActivity<ScanQrActivity>(
                            "alat" to data[position].alat,
                            "qty" to data[position].qty,
                            "status" to data[position].status_alat,
                            "idPinjaman" to data[position].id
                        )
                    }
                }
            }, main.activity!!, R.layout.list_detail_alat)
            Helper(main.activity, main.rvListAlat, adapter).setRecycleview()
        }catch (ex:Exception){
            Log.e("Error", ex.message.toString())
        }
    }

    fun getFormPengajuan() {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getDetailPeminjaman")
        val params = HashMap<String, String>()
        params["id_pengajuan"] = id_pengajuan

        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()

                    try{
                        val data = gson.fromJson(result, PengajuanModelResponse::class.java)
                        if (data.data.size > 0) {
                            main.edtTanggal.setText(data.data[0].tanggal_pinjam)
                            main.edtKet.setText(data.data[0].keterangan)
                        } else {
                            msg.showMessage("Data Tidak Ditemukan")
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun scanAlat(qrCode: String?) {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "doScanAlat")
        val params = HashMap<String, String>()
        params["id_pinjaman"] = sessionTrans.idPinjaman.toString()
        params["qrcode"] = qrCode.toString()
        params["alat"] = sessionTrans.alat.toString()
        params["qty"] = sessionTrans.qty.toString()
        params["status"] = sessionTrans.status.toString()

        Log.e("IdPengajuan", id_pengajuan)
        Log.e("qrCode", qrCode)

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DataScan", result)

                    sessionTrans.clearSession()
                    try{
                        val jObj = JSONObject(result)
                        if(jObj.get("is_valid").toString().equals("true")){
                            msg.showMessage("Scan Berhasil")
                            getListAlatDipinjam()
                        }else{
                            msg.showMessage(jObj.get("message").toString())
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    sessionTrans.clearSession()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun approveAsman(status: String) {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "approveAsman")
        val params = HashMap<String, String>()
        params["id"] = id_pengajuan
        params["status"] = status
        params["upt"] = session.uptId
        params["user"] = session.userId

        msg.showMessage("Proses Approve...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                @SuppressLint("RestrictedApi")
                override fun onSuccess(result: String) {
                    Log.e("ApproveAsman", result)
                    msg.hideLoading()
                    try{
                        val jObj = JSONObject(result)
                        if(jObj.get("is_valid").toString().equals("true")){
                            msg.showMessage("Approve Berhasil")
                            status_pinjam = "APPROVE"
                            main.btnReject.visibility = GONE
                            main.btnApprove.visibility = GONE
                        }else{
                            msg.showMessage(jObj.get("message").toString())
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }
}