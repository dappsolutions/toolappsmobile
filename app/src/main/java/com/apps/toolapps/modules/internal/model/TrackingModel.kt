package com.apps.toolapps.modules.internal.model

import com.google.gson.annotations.SerializedName

data class TrackingModel (
    @SerializedName("id")
    var id:String,
    @SerializedName("pegawai")
    var pegawai:String,
    @SerializedName("tgl_approve")
    var tgl_approve:String,
    @SerializedName("upt")
    var upt:String,
    @SerializedName("keterangan")
    var keterangan:String
)

data class TrackingModelResponse(val data:List<TrackingModel>)