package com.apps.toolapps.modules.internal.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.libraries.Session
import com.apps.toolapps.libraries.SessionTrans
import com.apps.toolapps.libraries.db
import com.apps.toolapps.libraries.models_db.DataAlatTB
import com.apps.toolapps.modules.internal.presenter.InternalPresenter
import kotlinx.android.synthetic.main.internal_view.*
import org.jetbrains.anko.db.delete
import java.nio.file.Files.delete

class Internal : Fragment(){
    lateinit var presenter:InternalPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.internal_view, container, false)
        return view
    }

    @SuppressLint("RestrictedApi")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = InternalPresenter(this)

        if(!Session(context).hakAkses.equals("REQUESTOR PEMINJAMAN ALAT")){
            btnAdd.visibility = GONE
        }

        if(presenter.jenis_pengajuan.equals("eksternal")){
            txtTitle.text = "Daftar Pengajuan Eksternal"
        }

        btnAdd.setOnClickListener {
            activity?.db?.use {
                delete(DataAlatTB.table_alat_upt, "${DataAlatTB.alat} != 0")
            }
            var formPengajuan =  FormInternal()
            val bundle = Bundle()
            bundle.putString("jenis", presenter.jenis_pengajuan)
            formPengajuan.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, formPengajuan)
        }

        btnCari.setOnClickListener {
            var cari = CariPengajuan()
            var bundle = Bundle()
            bundle.putString("jenis", presenter.jenis_pengajuan)
            cari.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, cari)
        }
    }
}