package com.apps.toolapps.modules.internal.presenter

import android.annotation.SuppressLint
import android.util.Log
import com.android.volley.VolleyError
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.internal.view.RejectPengajuan
import com.google.gson.Gson
import kotlinx.android.synthetic.main.detail_pengajuan_view.edtKet
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

class RejectPengajuanPresenter (val main:RejectPengajuan){
    var id_pengajuan: String
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        id_pengajuan = main.arguments!!.getString("id_pengajuan")
    }

    fun approveAsman(status: String) {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "approveAsman")
        val params = HashMap<String, String>()
        params["id"] = id_pengajuan
        params["status"] = status
        params["upt"] = session.uptId
        params["user"] = session.userId
        params["keterangan"] = main.edtKet.text.toString()

        msg.showMessage("Proses Rejected...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                @SuppressLint("RestrictedApi")
                override fun onSuccess(result: String) {
                    Log.e("ApproveAsman", result)
                    msg.hideLoading()
                    try{
                        val jObj = JSONObject(result)
                        if(jObj.get("is_valid").toString().equals("true")){
                            msg.showMessage("Reject Berhasil")
                            main.fragmentManager?.popBackStack()
                        }else{
                            msg.showMessage(jObj.get("message").toString())
                        }
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }
}