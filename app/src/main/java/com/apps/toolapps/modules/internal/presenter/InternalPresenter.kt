package com.apps.toolapps.modules.internal.presenter

import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.internal.model.PengajuanModel
import com.apps.toolapps.modules.internal.model.PengajuanModelResponse
import com.apps.toolapps.modules.internal.view.Detail
import com.apps.toolapps.modules.internal.view.Internal
import com.google.gson.Gson
import kotlinx.android.synthetic.main.internal_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.yesButton

class InternalPresenter(val main: Internal) {
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    lateinit var adapter: RvAdapter

    var jenis_pengajuan: String

    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()


        jenis_pengajuan = main.arguments?.getString("jenis").toString()
        getDataPeminjaman()
    }

    fun getDataPeminjaman() {
        main.contentStatusData.visibility = GONE
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getDataPeminjaman")
        val params = HashMap<String, String>()
        params["hak_akses"] = session.hakAkses

        params["status_dokumen"] = "2"
        if (jenis_pengajuan.equals("internal")) {
            params["status_dokumen"] = "1"
        }
        params["upt"] = session.uptId

        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Pengajuan", result)
                    msg.hideLoading()

                    try {
                        val data = gson.fromJson(result, PengajuanModelResponse::class.java)
                        if (data.data.size > 0) {
                            setDetailData(data.data)
                        } else {
                            main.contentStatusData.visibility = VISIBLE
//                        msg.showMessage("Data Tidak Ditemukan")
                        }
                    } catch (ex: Exception) {
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
//                    msg.showMessage(err)
                    main.contentStatusData.visibility = VISIBLE
                    main.imgStatus.setImageResource(R.drawable.ic_page_not_found)
                    main.txtStatusData.text = err
                }
            })
        }
    }

    fun setDetailData(data: List<PengajuanModel>) {
        try {
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtStatus = view.findViewById(R.id.txtStatus) as TextView
                    val txtNoPinjaman = view.findViewById(R.id.txtNoPinjaman) as TextView
                    val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                    val txtPeminjam = view.findViewById(R.id.txtPeminjam) as TextView
                    val txtUpt = view.findViewById(R.id.txtUpt) as TextView
                    val btnDetail = view.findViewById(R.id.btnDetail) as Button
                    val btnHapus = view.findViewById(R.id.btnHapus) as Button

                    txtStatus.text = data[position].status
                    txtNoPinjaman.text = data[position].no_peminjaman
                    txtTanggal.text = data[position].tanggal_pinjam
                    txtPeminjam.text = data[position].nama_pegawai
                    txtUpt.text = data[position].upt

                    btnDetail.setOnClickListener {
                        val bundle = Bundle()
                        bundle.putString("id_pengajuan", data[position].id)
                        bundle.putString("status", data[position].status)
                        bundle.putString("jenis", jenis_pengajuan)
                        val detail = Detail()
                        detail.arguments = bundle

                        Application().moveFragment(main.activity, R.id.frameContent, detail)
                    }

                    if (!data[position].status.equals("DRAFT")) {
                        btnHapus.visibility = GONE
                    } else {
                        if (!session.hakAkses.equals("REQUESTOR PEMINJAMAN ALAT")) {
                            btnHapus.visibility = GONE
                        }
                    }

                    btnHapus.setOnClickListener {
                        main.alert("Apa anda yakin menghapus Data ini") {
                            yesButton {
                                execDelete(data, position)
                            }

                            noButton {
                                it.dismiss()
                            }
                        }.show()
                    }
                }
            }, main.activity!!, R.layout.list_pengajuan)
            Helper(main.activity, main.rvListPengajuan, adapter).setRecycleview()
        } catch (ex: Exception) {
            Log.e("error", ex.message.toString())
        }
    }

    fun execDelete(data: List<PengajuanModel>, position: Int) {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "execDelete")
        val params = HashMap<String, String>()
        params["id_pinjaman"] = data[position].id

        msg.showMessage("Proses Hapus Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    msg.hideLoading()
                    val lisData = data as ArrayList<Any?>
                    lisData.removeAt(position)
                    adapter.notifyDataSetChanged()
                    msg.showMessage("Berhasil Dihapus")
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }
}