package com.apps.toolapps.modules.notification.model

import com.google.gson.annotations.SerializedName

data class NotifModel (
    @SerializedName("id")
    var id:String,
    @SerializedName("id_destination")
    var id_destination:String,
    @SerializedName("document")
    var document:String,
    @SerializedName("status")
    var status:String,
    @SerializedName("jenis")
    var jenis:String,
    @SerializedName("user")
    var user:String,
    @SerializedName("pesan")
    var pesan:String
)

data class NotifModelResponse(val data:List<NotifModel>)