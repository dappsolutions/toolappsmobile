package com.apps.toolapps.modules.internal.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.internal.presenter.RejectPengajuanPresenter
import kotlinx.android.synthetic.main.reject_keterangan_view.*

class RejectPengajuan : Fragment(){
    lateinit var presenter:RejectPengajuanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.reject_keterangan_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = RejectPengajuanPresenter(this)

        btnReject.setOnClickListener {
            presenter.approveAsman("REJECT")
        }
    }
}