package com.apps.toolapps.modules.internal.presenter

import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import android.view.View
import android.view.View.GONE
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.libraries.models_db.DataAlatTB
import com.apps.toolapps.modules.internal.model.AlatUptModel
import com.apps.toolapps.modules.internal.model.AlatUptModelResponse
import com.apps.toolapps.modules.internal.view.ListAlatUpt
import com.google.gson.Gson
import kotlinx.android.synthetic.main.alat_upt_view.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.doAsync
import org.json.JSONArray
import org.json.JSONObject

class ListAlatUptPresenter(val main: ListAlatUpt) {
    val upt: String

    var alat_terpilih = 0
    val baseUrl = ApiService.URL.BaseURl
    val network: Network
    val msg: Message
    val errmsg: Error
    val session: Session
    val gson: Gson

    lateinit var adapter: RvAdapter

    var jsonArray: JSONArray

    init {
        upt = main.arguments!!.getString("upt")

        network = Network(main.context)
        msg = Message(main.context)
        errmsg = Error()
        session = Session(main.context)
        gson = Gson()
        jsonArray = JSONArray()

        getListAlat()
    }

    private fun getListAlat() {
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getListalat")
        val params = HashMap<String, String>()
        params["upt"] = upt

        msg.showLoading("Proses Retrieving Data Alat...")
        doAsync {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("DataAlat", result)
                    msg.hideLoading()

                    try {
                        val data = gson.fromJson(result, AlatUptModelResponse::class.java)
                        setDetailData(data.data)
                    } catch (ex: Exception) {
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setDetailData(data: List<AlatUptModel>) {
        try {
            adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
                override fun onExecuteViewHolder(view: View, position: Int) {
                    val txtNo = view.findViewById(R.id.txtNo) as TextView
                    val txtKode = view.findViewById(R.id.txtKodeAlat) as TextView
                    val txtStok = view.findViewById(R.id.txtStok) as TextView
                    val edtQty = view.findViewById(R.id.edtQty) as EditText
                    val imMinus = view.findViewById(R.id.imgMinus) as ImageView
                    val imPlus = view.findViewById(R.id.imgPlus) as ImageView
                    val btnPilih = view.findViewById(R.id.btnPilih) as Button


                    txtNo.text = (position + 1).toString()
                    txtKode.text = data[position].kode_alat
                    txtStok.text = "Stok (" + data[position].stok + ")"

                    if (data[position].stok.equals("0")) {
                        edtQty.visibility = GONE
                        btnPilih.setText("Habis")
                        imPlus.visibility = GONE
                        imMinus.visibility = GONE
                        btnPilih.setBackgroundResource(R.drawable.round_danger)
                    }

                    imPlus.setOnClickListener {
                        var stok = edtQty.text.toString().toInt()
                        stok += 1
                        edtQty.setText(stok.toString())
                    }

                    imMinus.setOnClickListener {
                        var stok = edtQty.text.toString().toInt()
                        if (stok > 1) {
                            stok -= 1
                            edtQty.setText(stok.toString())
                        }
                    }


                    btnPilih.setOnClickListener {
                        var title = btnPilih.text.toString().toLowerCase()
                        if (title.equals("pilih")) {
                            btnPilih.setText("Terplih")
                            btnPilih.setBackgroundResource(R.drawable.round_warning)
                            alat_terpilih += 1
                            main.txtPilih.text = "Jumlah Terplih : " + alat_terpilih.toString()

                            val jsonParams = JSONObject()
                            jsonParams.put("no", position.toString())
                            jsonParams.put("alat", data[position].id)
                            jsonParams.put("kode_alat", data[position].kode_alat)
                            jsonParams.put("qty", edtQty.text.toString())
                            jsonParams.put("upt", upt)
                            jsonArray.put(jsonParams)

                        }

                        if (title.equals("terplih")) {
                            btnPilih.setText("Pilih")
                            btnPilih.setBackgroundResource(R.drawable.round_success)
                            alat_terpilih -= 1
                            main.txtPilih.text = "Jumlah Terplih : " + alat_terpilih.toString()

                            jsonArray.remove(position)
                        }
                    }

                }
            }, main.activity!!, R.layout.list_alat_upt)
            Helper(main.activity, main.rvListAlat, adapter).setRecycleview()
        } catch (ex: Exception) {
            Log.e("Error", ex.message.toString())
        }
    }

    fun simpan() {
        if (alat_terpilih > 0) {
            for (i in 0..jsonArray.length() - 1) {
                val jObj = jsonArray.getJSONObject(i)
                Log.e("DataJSON", jObj.toString())

                try {
                    main.activity?.db?.use {
                        insert(
                            DataAlatTB.table_alat_upt,
                            DataAlatTB.alat to jObj.getString("alat"),
                            DataAlatTB.kode_alat to jObj.getString("kode_alat"),
                            DataAlatTB.upt to jObj.getString("upt"),
                            DataAlatTB.qty to jObj.getString("qty")
                        )
                    }
                } catch (ex: SQLiteConstraintException) {
                    Log.e("ErrorSql", ex.message.toString())
                }
            }
            main.activity!!.onBackPressed()
        } else {
            msg.showMessage("Tidak Ada Alat yang dipilih")
        }
    }


}