package com.apps.toolapps.modules.dashboard.presenter

import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.dashboard.view.Dashboard
import com.google.gson.Gson
import kotlinx.android.synthetic.main.dashboard_view.*

class DashboardPresenter (val main:Dashboard){
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    lateinit var adapter: RvAdapter

    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        loadDashboard()

//        getDataAlatKeluar()
//        if(session.hakAkses.equals("TOOLSMAN")){
//            getDataAlatKembali()
//            main.contentAlatKembali.visibility = VISIBLE
//        }
    }

    fun loadDashboard() {
        main.webViewDashboard.settings.javaScriptEnabled = true
        main.webViewDashboard.webViewClient = WebClient(main.progress)
        main.webViewDashboard.loadUrl(ApiService.URL.BaseURlWeb)
    }

//    fun getDataAlatKeluar(){
//        main.contentStatusData.visibility = View.GONE
//        val url = ApiService.URL.baseUrl(baseUrl, "dashboard", "getDataAlatKeluar")
//        val params = HashMap<String, String>()
//        params["upt"] = session.uptId
//
//        msg.showLoading("Proses Retrieving Data...")
//        GlobalScope.launch(Dispatchers.Main) {
//            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
//                override fun onSuccess(result: String) {
//                    Log.e("Pengajuan", result)
//                    msg.hideLoading()
//
//                    val data = gson.fromJson(result, DataAlatKeluarResponse::class.java)
//                    if (data.data.size > 0) {
//                        setDetailData(data.data)
//                    } else {
//                        main.contentStatusData.visibility = View.VISIBLE
////                        msg.showMessage("Data Tidak Ditemukan")
//                    }
//                }
//
//                override fun onError(error: VolleyError) {
//                    msg.hideLoading()
//                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
////                    msg.showMessage(err)
//                    main.contentStatusData.visibility = View.VISIBLE
//                    main.imgStatus.setImageResource(R.drawable.ic_page_not_found)
//                    main.txtStatusData.text = err
//                }
//            })
//        }
//    }
//
//    fun setDetailData(data: List<DataAlatKeluar>) {
//        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
//            override fun onExecuteViewHolder(view: View, position: Int) {
//                val txtStatus = view.findViewById(R.id.txtStatus) as TextView
//                val txtNoPinjaman = view.findViewById(R.id.txtNoPinjaman) as TextView
//                val txtKodeAlat = view.findViewById(R.id.txtKodeAlat) as TextView
//                val txtTanggal = view.findViewById(R.id.txtTanggalPinjamn) as TextView
//                val txtPeminjam = view.findViewById(R.id.txtPeminjam) as TextView
//                val txtUpt = view.findViewById(R.id.txtUpt) as TextView
//                val btnStatus = view.findViewById(R.id.btnStatus) as Button
//
//                txtStatus.text = data[position].status
//                txtNoPinjaman.text = data[position].no_peminjaman
//                txtTanggal.text = data[position].tanggal_pinjam
//                txtKodeAlat.text = data[position].kode_alat
//                txtPeminjam.text = data[position].nama_pegawai
//                txtUpt.text = data[position].nama_upt
//
//                btnStatus.setText(data[position].status_alat)
//            }
//
//        }, main.activity!!, R.layout.list_alat_keluar)
//        Helper(main.activity, main.rvListAlatKeluar, adapter).setRecycleview()
//    }
//
//    fun getDataAlatKembali(){
//        main.contentStatusData.visibility = View.GONE
//        val url = ApiService.URL.baseUrl(baseUrl, "dashboard", "getDataAlatKembali")
//        val params = HashMap<String, String>()
//        params["upt"] = session.uptId
//
//        msg.showLoading("Proses Retrieving Data...")
//        GlobalScope.launch(Dispatchers.Main) {
//            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
//                override fun onSuccess(result: String) {
//                    Log.e("Pengajuan", result)
//                    msg.hideLoading()
//
//                    val data = gson.fromJson(result, DataAlatKembaliResponse::class.java)
//                    if (data.data.size > 0) {
//                        setDetailDataKembali(data.data)
//                    } else {
//                        main.contentStatusData.visibility = VISIBLE
////                        msg.showMessage("Data Tidak Ditemukan")
//                    }
//                }
//
//                override fun onError(error: VolleyError) {
//                    msg.hideLoading()
//                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
////                    msg.showMessage(err)
//                    main.contentStatusData.visibility = View.VISIBLE
//                    main.imgStatus.setImageResource(R.drawable.ic_page_not_found)
//                    main.txtStatusData.text = err
//                }
//            })
//        }
//    }
//
//    fun setDetailDataKembali(data: List<DataAlatKembali>) {
//        adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
//            override fun onExecuteViewHolder(view: View, position: Int) {
//                val txtStatus = view.findViewById(R.id.txtStatus) as TextView
//                val txtNoPinjaman = view.findViewById(R.id.txtNoPinjaman) as TextView
//                val txtKodeAlat = view.findViewById(R.id.txtKodeAlat) as TextView
//                val txtTanggal = view.findViewById(R.id.txtTanggalPinjamn) as TextView
//                val txtPeminjam = view.findViewById(R.id.txtPeminjam) as TextView
//                val txtUpt = view.findViewById(R.id.txtUpt) as TextView
//                val btnStatus = view.findViewById(R.id.btnStatus) as Button
//
//                txtStatus.text = data[position].status
//                txtNoPinjaman.text = data[position].no_peminjaman
//                txtTanggal.text = data[position].tanggal_pinjam
//                txtKodeAlat.text = data[position].kode_alat
//                txtPeminjam.text = data[position].nama_pegawai
//                txtUpt.text = data[position].nama_upt
//
//                btnStatus.setText(data[position].status_alat)
//            }
//
//        }, main.activity!!, R.layout.list_alat_kembali)
//        Helper(main.activity, main.rvListAlatKembali, adapter).setRecycleview()
//    }
}