package com.apps.toolapps.modules.surat_pinjaman.presenter

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.util.Base64
import android.util.Log
import android.widget.ArrayAdapter
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.internal.model.UptModel
import com.apps.toolapps.modules.internal.model.UptModelResponse
import com.apps.toolapps.modules.surat_pinjaman.model.StatusPinjamModel
import com.apps.toolapps.modules.surat_pinjaman.model.StatusPinjamModelResponse
import com.apps.toolapps.modules.surat_pinjaman.view.FormSuratPinjam
import com.google.gson.Gson
import kotlinx.android.synthetic.main.form_surat_pinjam_view.*
import kotlinx.android.synthetic.main.form_surat_pinjam_view.spUpt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.File
import java.io.FileInputStream

class FormSuratPresenter(val main: FormSuratPinjam) {
    val select_file = 1
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    var listUpt: ArrayList<String>
    var listStatus: ArrayList<String>

    var id_pinjaman:String
    init {
        id_pinjaman = main.arguments!!.getString("id_pinjaman").toString()
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()
        listUpt = ArrayList()
        listStatus = ArrayList()

        if (!checkSelftPermission()) {
            try {
                ActivityCompat.requestPermissions(
                    main.activity!!,
                    arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    1
                )
            } catch (ex: Exception) {
                Log.e("ErrorPer", ex.message.toString())
            }
        }

        getListUpt()
        getListStatus()
    }

    fun browseFile() {
        val files = Intent()
        files.type = "application/pdf"
        files.action = Intent.ACTION_GET_CONTENT
        files.addCategory(Intent.CATEGORY_OPENABLE)
        main.startActivityForResult(Intent.createChooser(files, "Select Files"), select_file)
    }

    fun selectFile(data: Intent?) {
        val path = data?.data
//        val uriString = path.toString()
//        val myFile = File(uriString)

//        var name = myFile.name.replace("primary%3A", "")
//        name = name.replace("%20", " ")
//        Log.e("File", name)

//        main.txtNamaFile.text = name
//        main.file_name = name

        var dataPath = FilePath().getPath(main.context!!, path!!)
        Log.e("DataPath", dataPath)

        if (path != null) {
            try {

                val dataGambar = dataPath?.split("/")
                main.txtNamaFile.text = dataGambar!![dataGambar.size - 1]
                main.file_name = dataGambar!![dataGambar.size - 1]

                val fileSaya = File(dataPath)
                val inputStream = FileInputStream(fileSaya)
                val bytes = ByteArray(fileSaya.length().toInt() - 1)
                inputStream.read(bytes)
                Log.e("InputStream", inputStream.toString())

                var base64 = Base64.encodeToString(bytes, Base64.DEFAULT)

                main.base64File = base64
            } catch (ex: Exception) {
                Log.e("Error", ex.message)
            }
        }
    }

    fun checkSelftPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            var result = main.context?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            return result == PackageManager.PERMISSION_GRANTED
        }

        return false
    }

    fun simpan() {
        var url = ApiService.URL.baseUrl(baseUrl, "surat_pinjam", "simpan")
        var params = HashMap<String, String>()
        params["file_name"] = main.file_name
        params["base64_file"] = main.base64File
        params["id_pinjaman"] = id_pinjaman
        params["upt"] = main.spUpt.selectedItem.toString()
        params["status"] = main.spStatus.selectedItem.toString()

        msg.showLoading("Proses Simpan Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Save", result)
                    msg.hideLoading()
                    try{
                        val jObj = JSONObject(result)
                        if(jObj.get("is_valid").toString().equals("true")){
                            msg.showMessage("Proses Upload Surat Berhasil")
                            main.fragmentManager?.popBackStack()
                        }
                    }catch (ex:Exception){
                        Log.e("Error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun getListUpt() {
        val url = ApiService.URL.baseUrl(baseUrl, "surat_pinjam", "getListUpt")
        val params = HashMap<String, String>()

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    val data = gson.fromJson(result, UptModelResponse::class.java)
                    setListUpt(data.data)
                }

                override fun onError(error: VolleyError) {
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setListUpt(data: List<UptModel>) {
        listUpt.clear()
        listUpt.add("--Pilih Upt--")
        for(i in 0..data.size -1){
            listUpt.add(data[i].upt)
        }

        val adapterList = ArrayAdapter<String>(main.context, R.layout.list_spinner, listUpt)
        adapterList.setDropDownViewResource(R.layout.list_spinner)
        main.spUpt.adapter = adapterList
    }

    fun getListStatus() {
        val url = ApiService.URL.baseUrl(baseUrl, "surat_pinjam", "getListStatusPeminjaman")
        val params = HashMap<String, String>()

        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    val data = gson.fromJson(result, StatusPinjamModelResponse::class.java)
                    setListStatus(data.data)
                }

                override fun onError(error: VolleyError) {
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
                    msg.showMessage(err)
                }
            })
        }
    }

    fun setListStatus(data: List<StatusPinjamModel>) {
        listStatus.clear()
        listStatus.add("--Pilih Status--")
        for(i in 0..data.size -1){
            listStatus.add(data[i].status)
        }

        val adapterList = ArrayAdapter<String>(main.context, R.layout.list_spinner, listStatus)
        adapterList.setDropDownViewResource(R.layout.list_spinner)
        main.spStatus.adapter = adapterList
    }
}