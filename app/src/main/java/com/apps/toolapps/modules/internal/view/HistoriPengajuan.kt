package com.apps.toolapps.modules.internal.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.modules.internal.presenter.HistoriPengajuanPresenter
import kotlinx.android.synthetic.main.internal_view.*

class HistoriPengajuan :Fragment(){
    lateinit var presenter:HistoriPengajuanPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.histori_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = HistoriPengajuanPresenter(this)

        if(presenter.jenis_pengajuan.equals("eksternal")){
            txtTitle.text = "Histori Pengajuan Eksternal"
        }

        btnCari.setOnClickListener {
            var cari = CariHistoryData()
            var bundle = Bundle()
            bundle.putString("jenis", presenter.jenis_pengajuan)
            cari.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, cari)
        }
    }
}