package com.apps.toolapps.modules.internal.presenter

import android.util.Log
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.Button
import android.widget.TextView
import com.android.volley.VolleyError
import com.apps.toolapps.R
import com.apps.toolapps.libraries.*
import com.apps.toolapps.modules.internal.model.TrackingModel
import com.apps.toolapps.modules.internal.model.TrackingModelResponse
import com.apps.toolapps.modules.internal.view.TrackingPengajuan
import com.google.gson.Gson
import kotlinx.android.synthetic.main.tracking_pengajuan_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class TrackingPengajuanPresenter (val main:TrackingPengajuan){
    val baseUrl = ApiService.URL.BaseURl
    val helper: Helper
    val session: Session
    val network: Network
    val errmsg: Error
    val gson: Gson
    val msg: Message
    lateinit var adapter: RvAdapter
    var id_pengajuan: String
    init {
        helper = Helper(main.activity)
        msg = Message(main.context)
        session = Session(main.context)
        network = Network(main.context)
        errmsg = Error()
        gson = Gson()

        id_pengajuan = main.arguments!!.getString("id_pengajuan")
        getListStatusPengajuan()
    }

    fun getListStatusPengajuan() {
        main.contentStatusData.visibility = GONE
        val url = ApiService.URL.baseUrl(baseUrl, "pinjam", "getListStatusPengajuan")
        val params = HashMap<String, String>()
        params["id"] = id_pengajuan
        params["hak_akses"] = session.hakAkses
        params["upt"] = session.uptId


        msg.showLoading("Proses Retrieving Data...")
        GlobalScope.launch(Dispatchers.Main) {
            network.volleyRequestWithErrorCustom(url, params, object : VolleyCallbackWithError {
                override fun onSuccess(result: String) {
                    Log.e("Pengajuan", result)
                    msg.hideLoading()

                    try{
                        val data = gson.fromJson(result, TrackingModelResponse::class.java)
                        if (data.data.size > 0) {
                            setDetailData(data.data)
                        } else {
                            main.contentStatusData.visibility = VISIBLE
//                        msg.showMessage("Data Tidak Ditemukan")
                        }
                    }catch (ex:Exception){
                        Log.e("error", ex.message.toString())
                    }
                }

                override fun onError(error: VolleyError) {
                    msg.hideLoading()
                    val err = errmsg.checkOnErrorVolleyNetworkWithMessage(error)
//                    msg.showMessage(err)
                    main.contentStatusData.visibility = VISIBLE
                    main.imgStatus.setImageResource(R.drawable.ic_page_not_found)
                    main.txtStatusData.text = err
                }
            })
        }
    }

    fun setDetailData(data: List<TrackingModel>) {
       try{
           adapter = RvAdapter(data as ArrayList<Any?>, object : ListCallback {
               override fun onExecuteViewHolder(view: View, position: Int) {
                   val txtNamaApprove = view.findViewById(R.id.txtNamaApprove) as TextView
                   val txtJabatan = view.findViewById(R.id.txtJabatan) as TextView
                   val txtTanggal = view.findViewById(R.id.txtTanggal) as TextView
                   val btnStatus = view.findViewById(R.id.btnStatus) as Button

                   txtNamaApprove.text = data[position].pegawai
                   txtJabatan.text = "ASMAN "+data[position].upt
                   txtTanggal.text = data[position].tgl_approve

               }
           }, main.activity!!, R.layout.list_tracking)
           Helper(main.activity, main.rvListTracking, adapter).setRecycleview()
       }catch (ex:Exception){
           Log.e("error", ex.message.toString())
       }
    }


}