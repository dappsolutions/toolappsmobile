package com.apps.toolapps.modules.surat_pinjaman.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.apps.toolapps.R
import com.apps.toolapps.modules.surat_pinjaman.presenter.DocumentSuratPresenter

class DocumentSuratAct : AppCompatActivity() {

    lateinit var presenter:DocumentSuratPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.document_surat_view)

        presenter = DocumentSuratPresenter(this)
    }
}
