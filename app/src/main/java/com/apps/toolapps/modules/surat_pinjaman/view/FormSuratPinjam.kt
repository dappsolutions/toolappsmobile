package com.apps.toolapps.modules.surat_pinjaman.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.surat_pinjaman.presenter.FormSuratPresenter
import kotlinx.android.synthetic.main.form_surat_pinjam_view.*
class FormSuratPinjam : Fragment(){
    lateinit var presenter:FormSuratPresenter
    var file_name:String = ""
    var base64File:String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.form_surat_pinjam_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = FormSuratPresenter(this)

        btnAttacth.setOnClickListener {
            presenter.browseFile()
        }

        btnSimpan.setOnClickListener {
            presenter.simpan()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == presenter.select_file){
                presenter.selectFile(data)
            }
        }
    }
}