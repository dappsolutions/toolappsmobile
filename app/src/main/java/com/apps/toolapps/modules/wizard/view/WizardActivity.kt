package com.apps.toolapps.modules.wizard.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.apps.toolapps.MainActivity
import com.apps.toolapps.R
import com.apps.toolapps.modules.wizard.presenter.WizardPresenter
import org.jetbrains.anko.startActivity

class WizardActivity : AppCompatActivity() {

    lateinit var presenter:WizardPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wizard)

        presenter = WizardPresenter(this)
    }
}
