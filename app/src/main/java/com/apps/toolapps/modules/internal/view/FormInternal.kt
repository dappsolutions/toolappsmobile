package com.apps.toolapps.modules.internal.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.modules.internal.presenter.FormInternPresenter
import com.apps.toolapps.modules.surat_pinjaman.view.SuratPinjaman
import kotlinx.android.synthetic.main.form_internal_view.*


class FormInternal : Fragment() {

    lateinit var presenter: FormInternPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.form_internal_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = FormInternPresenter(this)

        presenter.setTanggal()

        if(presenter.jenis_pengajuan.equals("eksternal")){
            txtTitle.text = "Form Pengajuan Eksternal"
        }

        btnUpload.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("id_pinjaman", presenter.pinjaman_id)
            bundle.putString("status", presenter.status_pinjam)
            val surat = SuratPinjaman()
            surat.arguments = bundle
            Application().moveFragment(activity, R.id.frameContent, surat)
        }

        btnSimpan.setOnClickListener {
            presenter.simpan()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.getListAlatUpt()
    }
}