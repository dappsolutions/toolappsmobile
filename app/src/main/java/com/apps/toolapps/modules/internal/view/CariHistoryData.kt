package com.apps.toolapps.modules.internal.view

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.internal.presenter.CariHistoryPresenter
import kotlinx.android.synthetic.main.cari_pengajuan_view.*
import org.jetbrains.anko.support.v4.alert

class CariHistoryData : Fragment(){
    lateinit var presenter:CariHistoryPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.cari_data_history_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = CariHistoryPresenter(this)

        btnFilter.setOnClickListener {
            if(!TextUtils.isEmpty(edtKeyword.text)){
                presenter.getDataPeminjaman()
            }else{
                alert ("Pencarian Harus Diisi"){

                }.show()
            }
        }
    }
}