package com.apps.toolapps.modules.wizard.presenter

import android.os.Handler
import android.util.Log
import com.apps.toolapps.MainActivity
import com.apps.toolapps.R
import com.apps.toolapps.libraries.Application
import com.apps.toolapps.libraries.Session
import com.apps.toolapps.modules.login.view.Login
import com.apps.toolapps.modules.main_app.view.MainAppAct
import com.apps.toolapps.modules.wizard.view.WizardActivity
import org.jetbrains.anko.startActivity

class WizardPresenter (val main:WizardActivity){
    init {
        checkingNotification()
    }

    private fun checkingNotification() {
        if (!Session(main).userId.equals("missing")) {
            if (main.intent.extras != null) {
                Log.e("InitHal", "Notif")
                val id_destination = main.intent.getStringExtra("id_destination")
                val document = main.intent.getStringExtra("document")
                if (id_destination != null) {
                    main.finish()
                    when (document) {
                        "pinjaman" -> {
                            val status = main.intent.getStringExtra("status")
                            val jenis = main.intent.getStringExtra("jenis")
                            main.startActivity<MainAppAct>(
                                "id_destination" to id_destination,
                                "document" to document,
                                "status" to status,
                                "jenis" to jenis)
                        }
                    }
                } else {
                    main.finish()
                    main.startActivity<MainAppAct>()
                }
            } else {
                Log.e("Notif", "Tidak ADa")
                main.finish()
                main.startActivity<MainAppAct>()
            }
        } else {
            Log.e("InitHal", "No Notif")
            var handler = Handler()
            handler.postDelayed(object : Runnable {
                override fun run() {
                    main.finish()
                    initContent()
                }
            }, 5000L)
        }
    }

    fun initContent() {
        main.finish()
        main.startActivity<MainActivity>()
    }
}