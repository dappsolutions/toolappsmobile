package com.apps.toolapps.modules.dashboard.view

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apps.toolapps.R
import com.apps.toolapps.modules.dashboard.presenter.DashboardPresenter

class Dashboard : Fragment(){
    lateinit var presenter:DashboardPresenter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view  = inflater.inflate(R.layout.dashboard_view, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = DashboardPresenter(this)

        //back press
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener(object : View.OnKeyListener{
            override fun onKey(v: View?, keyCode: Int, event: KeyEvent?): Boolean {
                if(keyCode == KeyEvent.KEYCODE_BACK){
                    val homeScreen = Intent(Intent.ACTION_MAIN)
                    homeScreen.addCategory(Intent.CATEGORY_HOME)
                    startActivity(homeScreen)
                    return true
                }
                return false
            }
        })
    }
}